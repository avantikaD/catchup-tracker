package com.mediaiq;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Meeting.class)
public abstract class Meeting_ {

	public static volatile SingularAttribute<Meeting, Attendee> primaryAttendee;
	public static volatile ListAttribute<Meeting, Attendee> secondaryAttendee;
	public static volatile SingularAttribute<Meeting, String> name;
	public static volatile SingularAttribute<Meeting, Long> id;

}

