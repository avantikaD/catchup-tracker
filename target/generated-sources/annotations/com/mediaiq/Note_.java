package com.mediaiq;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Note.class)
public abstract class Note_ {

	public static volatile SingularAttribute<Note, Long> id;
	public static volatile SingularAttribute<Note, String> text;
	public static volatile SingularAttribute<Note, Meeting> meeting;

}

