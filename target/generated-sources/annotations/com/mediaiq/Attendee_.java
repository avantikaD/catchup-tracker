package com.mediaiq;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Attendee.class)
public abstract class Attendee_ {

	public static volatile SingularAttribute<Attendee, String> name;
	public static volatile SingularAttribute<Attendee, Long> id;
	public static volatile SingularAttribute<Attendee, String> email;

}

