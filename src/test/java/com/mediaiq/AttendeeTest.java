//package com.mediaiq;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.SpringApplicationConfiguration;
//import org.springframework.context.annotation.Import;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = {CatchupTrackerApplication.class})
//public class AttendeeTest {
//
//	@Autowired
//	private AttendeeRepository attendeeRepository;
//	
//	public void setAttendeeRepository(AttendeeRepository attendeeRepository) {
//		this.attendeeRepository = attendeeRepository;
//	}
//	@Test
//	public void createAttendee(){
//		
//		Attendee attendee1 = new Attendee();
//		attendee1.setName("attendee1");
//		attendee1.setEmail("a1@miq");
//		attendeeRepository.save(attendee1);
//		
//		Attendee attendee2 = new Attendee();
//		attendee2.setName("attendee2");
//		attendee2.setEmail("a2@miq");
//		attendeeRepository.save(attendee2);
//		
//		Attendee attendee3 = new Attendee();
//		attendee3.setName("attendee3");
//		attendee3.setEmail("a3@miq");
//		attendeeRepository.save(attendee3);
//		
//		Attendee attendee4 = new Attendee();
//		attendee4.setName("attendee4");
//		attendee4.setEmail("a4@miq");		
//		attendeeRepository.save(attendee4);
//	}
//	
//}
