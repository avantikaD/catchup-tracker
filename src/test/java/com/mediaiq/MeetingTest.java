//package com.mediaiq;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(classes = {CatchupTrackerApplication.class})
//public class MeetingTest {
//
//	
//	private MeetingRepository meetingRepository;
//	@Autowired
//	private AttendeeRepository attendeeRepo;
//
//	@Autowired
//	public void setMeetingRepository(MeetingRepository meetingRepository) {
//		this.meetingRepository = meetingRepository;
//	}
//	
//	@Test
//	public void createMeeting(){
//		
//		Meeting meeting1 = new Meeting();
//		meeting1.setName("meet1");
//		Attendee attendee1 = attendeeRepo.findOne(1l);
//		meeting1.setPrimaryAttendee(attendee1);
//		
//		Attendee secAttendee1 = attendeeRepo.findOne(3l);
//		Attendee secAttendee2 = attendeeRepo.findOne(4l);
//		
//		
//		Meeting meeting2 = new Meeting();
//		meeting2.setName("meet2");
//		Attendee attendee2 = attendeeRepo.findOne(2l);
//		meeting2.setPrimaryAttendee(attendee2);
//		
//		
//		List<Attendee> secAttendeeList = new ArrayList<Attendee>();
//		secAttendeeList.add(secAttendee1);
//		secAttendeeList.add(secAttendee2);		
//		meeting1.setSecondaryAttendee(secAttendeeList);
//		meeting2.setSecondaryAttendee(secAttendeeList);
//		
//		meetingRepository.save(meeting1);
//		meetingRepository.save(meeting2);
//		
//	}
//	
//	@Test
//	public void addMeeting(){
//		
//		Meeting meeting3 = new Meeting();
//		meeting3.setName("meet3");
//		Attendee attendee3 = attendeeRepo.findOne(1l);
//		meeting3.setPrimaryAttendee(attendee3);
//		
//		Attendee secAttendee1 = attendeeRepo.findOne(3l);
//		Attendee secAttendee2 = attendeeRepo.findOne(4l);
//		List<Attendee> secAttendeeList = new ArrayList<Attendee>();
//		secAttendeeList.add(secAttendee1);
//		secAttendeeList.add(secAttendee2);
//		
//		meeting3.setSecondaryAttendee(secAttendeeList);
//		
//		meetingRepository.save(meeting3);
//		
//		
//	}
//	
//	
////	@Test
////	public void testSaveMeeting(){
////		Meeting meeting = new Meeting();
////		Attendee attendee = new Attendee();
//////		Attendee secAttendee1 = new Attendee();
//////		Attendee secAttendee2 = new Attendee();
//////		List<Attendee> secAttendeeList = new ArrayList<Attendee>();
////		
////		
//////		secAttendee1.setEmail("secemail1");
//////		secAttendee1.setName("secname1");
//////		secAttendee2.setEmail("secemail2");
//////		secAttendee2.setName("secname2");
//////		secAttendeeList.add(secAttendee1);
//////		secAttendeeList.add(secAttendee2);
////		
////		attendee.setEmail("test11");
////		attendee.setName("test11");
//////		
//////		meeting.setName("meet1");
//////		meeting.setPrimaryAttendee(attendee);
//////		meeting.setSecondaryAttendee(secAttendeeList);
//////		meetingRepository.save(meeting);
////		
//////		List<Meeting> meetings = meetingRepository.findAll();
////		
//////		Meeting fetchedMeeting = meetingRepository.findOne(meeting.getId());
//////		fetchedMeeting.setName("meet1again");
//////		meetingRepository.save(fetchedMeeting);
////		
////		Meeting meeting3 = new Meeting();
////		meeting3.setName("meet10");
////		meeting3.setPrimaryAttendee(attendee);
//////		Attendee fetchAttendee = attendeeRepo.findOne(2l);
//////		meeting3.setPrimaryAttendee(fetchAttendee);
////		meetingRepository.save(meeting3);
////		
////		Meeting meeting2 = new Meeting();
////		meeting2.setName("meet11");
////		meeting2.setPrimaryAttendee(attendee);
////		meetingRepository.save(meeting2);
////		
////		
////		
////		
////		
////		
////	}
//	
//	
//	
//}
