package com.mediaiq;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MeetingServiceImpl implements MeetingService{

	@Autowired
	private MeetingRepository meetingRepository;

	@Override
	public List<Meeting> findAll() {
		return meetingRepository.findAll();
	}


	@Override
	public List<Meeting> findByPrimaryAttendeeId(Long id) {
		return meetingRepository.findByPrimaryAttendeeId(id);
	}


	@Override
	public List<Meeting> findBySecondaryAttendeeId(Long id) {
		return meetingRepository.findBySecondaryAttendeeId(id);
	}


	@Override
	public Meeting findById(Long id) {
		return meetingRepository.findById(id); 
	}


	@Override
	public List<Attendee> getAttendee(Long id, String attendeeType) {
		
		List<Attendee> attendee = new ArrayList<Attendee>();
		Meeting meeting = meetingRepository.findById(id);
		if(attendeeType.equals("primary")){
			attendee.add(meeting.getPrimaryAttendee());
			return attendee ;
		}
		else if(attendeeType.equals("secondary")){
			return meeting.getSecondaryAttendee();
		}
		else 
			return null;
		
	}


	@Override
	public void save(Meeting meeting) {
		meetingRepository.save(meeting);
	}

	
}
