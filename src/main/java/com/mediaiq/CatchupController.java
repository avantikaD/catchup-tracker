package com.mediaiq;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CatchupController {
	
	@RequestMapping("/")
	public String login(){
		return "login";
	}
	
	@RequestMapping("/home")
	public String home(){
		return "home";
	}
}

