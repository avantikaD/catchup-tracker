package com.mediaiq;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AttendeeServiceImpl implements AttendeeService {

	@Autowired
	private AttendeeRepository attendeeRepository;
	
	@Override
	public List<Attendee> findAll() {
		return attendeeRepository.findAll();
	}

	@Override
	public List<Attendee> findById(Long id) {
		return attendeeRepository.findById(id);
	}

	@Override
	public void save(Attendee attendee) {
		attendeeRepository.save(attendee);
		
	}

	@Override
	public Long exists(String email) {
		Attendee attendee = attendeeRepository.findByEmail(email);
		if(attendee == null) {
			return -1l;	
		}
		else {
			Long attendeeId = attendee.getId();
			return attendeeId;	
		}
	}
	
//	@Override
//	public Long findByEmail(String email) {
//		Attendee attendee=attendeeRepository.findByEmail(email);
//		return attendee.getId();
//					
//	}

}
