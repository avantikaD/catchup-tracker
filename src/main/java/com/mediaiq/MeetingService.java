package com.mediaiq;

import java.util.List;

public interface MeetingService {

	public List<Meeting> findAll();

	public List<Meeting> findByPrimaryAttendeeId(Long id);

	public List<Meeting> findBySecondaryAttendeeId(Long secondaryattendeeid);

	public Meeting findById(Long id);

	public List<Attendee> getAttendee(Long id, String attendeeType);

	public void save(Meeting meeting);
}
