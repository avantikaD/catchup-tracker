package com.mediaiq;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

//@Configuration
//@EnableAutoConfiguration

public interface MeetingRepository extends JpaRepository<Meeting, Long>{

	//get all the meetings of an attendee
	List<Meeting> findAll();

	List<Meeting> findByPrimaryAttendeeId(Long id);

	List<Meeting> findBySecondaryAttendeeId(Long id);

	Meeting findById(Long id);
	
}
