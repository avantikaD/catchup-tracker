package com.mediaiq;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AttendeeRepository extends JpaRepository<Attendee, Long>{

	List<Attendee> findById(Long id);

	Attendee findByEmail(String email);
	
//	List<Attendee> findByName(String name);
	
}
