package com.mediaiq;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/note")
public class NoteController {

	@Autowired
	private NoteService noteService;
	
	//get notes for a meeting
	@RequestMapping(value="/meeting/{id}")
	public List<Note> getNoteByMeeting(@PathVariable Long id){
		List<Note> note = noteService.findByMeetingId(id);
		return note;
	}
	
	//create a note
	@RequestMapping(value="/", method=RequestMethod.POST)
	@ResponseBody
	public Note createNote(@RequestBody Note note){
		 noteService.save(note);
		 return note;				
	}
	
}
