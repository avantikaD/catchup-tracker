package com.mediaiq;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="meeting",catalog="catchup")
public class Meeting {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String name;
	
	@ManyToOne(fetch=FetchType.EAGER)
//	@OneToOne(cascade=CascadeType.ALL)
	private Attendee primaryAttendee;
	
//	@OneToMany(cascade=CascadeType.ALL)
	@ManyToMany(fetch=FetchType.EAGER)
	private List<Attendee> secondaryAttendee;
	
	
	public List<Attendee> getSecondaryAttendee() {
		return secondaryAttendee;
	}

	public void setSecondaryAttendee(List<Attendee> secondaryAttendee) {
		this.secondaryAttendee = secondaryAttendee;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Attendee getPrimaryAttendee() {
		return primaryAttendee;
	}

	public void setPrimaryAttendee(Attendee primaryAttendee) {
		this.primaryAttendee = primaryAttendee;
	}

	@Override
	public String toString() {
		return "Meeting [id=" + id + ", name=" + name + ", primaryAttendee=" + primaryAttendee + ", secondaryAttendee="
				+ secondaryAttendee + "]";
	}


	

	
}
