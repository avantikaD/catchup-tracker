package com.mediaiq;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface NoteRepository extends CrudRepository<Note, Long>{

	List<Note> findByMeetingId(Long id);

//	@Async

}
