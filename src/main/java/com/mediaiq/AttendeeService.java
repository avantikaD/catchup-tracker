package com.mediaiq;

import java.util.List;

public interface AttendeeService {

	public List<Attendee> findAll();

	public List<Attendee> findById(Long id);

	public void save(Attendee attendee);

	public Long exists(String email);

//	public Long findByEmail(String email);
	
//	@POST
	
}
