package com.mediaiq;

import java.util.List;

public interface NoteService {

	List<Note> findByMeetingId(Long id);

	void save(Note note);


}
