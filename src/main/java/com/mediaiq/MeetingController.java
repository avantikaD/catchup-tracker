package com.mediaiq;

import java.util.List;

import org.apache.log4j.spi.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/meeting")
public class MeetingController {
	
	Logger logger=org.slf4j.LoggerFactory.getLogger(MeetingController.class);
	@Autowired
	private MeetingService meetingService;
	
	//get all the meetings
	@RequestMapping(value="/")
	public List<Meeting> getAllMeetings(){
		
		List<Meeting> meetings = meetingService.findAll();
		return meetings;
	}
	
	//get all the meetings of a primary/secondary attendee
	@RequestMapping
	public List<Meeting> getMeetingOfAttendee(@RequestParam("attendeeType") String attendeeType, 
			@RequestParam("attendeeId") Long attendeeId){
		
		List<Meeting> meetingOfAttendee;
		if (attendeeType.equals("primary")){
			meetingOfAttendee = meetingService.findByPrimaryAttendeeId(attendeeId);
		}
		else if(attendeeType.equals("secondary")){
			meetingOfAttendee = meetingService.findBySecondaryAttendeeId(attendeeId);
		}
		else{
			meetingOfAttendee = null;
		}
		
		return meetingOfAttendee;
		
	}
	
	//get primary/secondary attendee of a meeting
	@RequestMapping(value="/{id}")
	public List<Attendee> getPrimaryAttendeeOfMeeting(@PathVariable Long id, 
			@RequestParam("attendeeType") String attendeeType){
		
		List<Attendee> attendee = meetingService.getAttendee(id, attendeeType);
		return attendee;
	}
	
	//create a meeting
	@RequestMapping(value="/", method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Meeting createMeeting(@RequestBody Meeting meeting) {
		logger.info("Saving meeting {}",meeting);
		meetingService.save(meeting);
		return meeting;
	}
	
}
