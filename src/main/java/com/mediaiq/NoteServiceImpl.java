package com.mediaiq;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NoteServiceImpl implements NoteService{

	@Autowired
	private NoteRepository noteRepository;
	
	@Override
	public List<Note> findByMeetingId(Long id) {
		return noteRepository.findByMeetingId(id);
	}

	@Override
	public void save(Note note) {
		noteRepository.save(note);
		
	}

}
