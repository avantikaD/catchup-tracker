package com.mediaiq;

import java.util.List;

import javax.ws.rs.Encoded;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/attendee")
public class AttendeeController {

	@Autowired
	private AttendeeService attendeeService;
	
	//get all the attendees
	@RequestMapping(value="/")
	public List<Attendee> getAllAttendee(){
		
		List<Attendee> attendees = attendeeService.findAll();
		return attendees;
	}
	
	//get details of an attendee given its email
	@RequestMapping(value="/{id}")
	public List<Attendee> getAttendeeById(@PathVariable Long id){
		
		List<Attendee> detail = attendeeService.findById(id);
		return detail;
	}
	
	//create attendee
	@RequestMapping(value="", method=RequestMethod.POST)
	public Attendee createAttendee(@RequestBody Attendee attendee){
		attendeeService.save(attendee);
		return attendee;
	}
	
	//check if an attendee already exists by returning it's id
	@RequestMapping(value="/email/{email}/")
	public Long attendeeExists(@PathVariable String email){
		Long attendeeId = attendeeService.exists(email);
		return attendeeId;
	}
	
	//get details of an attendee id given its email
//	@RequestMapping(value="/attendeeid/{email}")
//	public Long getAttendeeIdByEmail(@PathVariable String email){
//		
//		Long attendeeId = attendeeService.findByEmail(email);
//		return attendeeId;
//	}
	
}
