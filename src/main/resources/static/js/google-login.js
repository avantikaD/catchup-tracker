person = {};

function onSuccess(googleUser) {
    console.log('onSuccess!');
    var uname = googleUser.getBasicProfile().getName();
    var email = googleUser.getBasicProfile().getEmail();
    var regex = new RegExp("@mediaiqdigital.com");
    if(regex.test(email)) {
    	//check Attendee db if present
    	$("#signinmsg").html("<p>" + uname + " is signed in.</p>");
    	
    	window.location.href = "/home";
    }
    else {
    	$("#signinmsg").html("<p>You are not authorized to use this app!!</p>");
    }
    
    localStorage.setItem("name", uname);
    localStorage.setItem("email", email);
    /*
    person = {
   		"name": uname,
    	"email": email
    };
    */
    //module.exports = person;
}

function onFailure(error) {
  console.log(error);
}

function signOut() {
  var auth2 = gapi.auth2.getAuthInstance();
  auth2.signOut().then(function () {
    console.log('User signed out.');
    
    $("#signinmsg").html("<p>" + "Signed out.</p>");
    
  });
}

function renderButton() {
  gapi.signin2.render('my-signin2', {
    'scope': 'profile email',
    'width': "240%",
    'height': "50%",
    'longtitle': true,
    'theme': 'dark',
    'onsuccess': onSuccess,
    'onfailure': onFailure
  });
}
