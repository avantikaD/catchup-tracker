var homeModule = angular.module("home", []);

homeModule.controller("homeController", function($scope, $http) {
	
	var personInfo = {
		name : localStorage.getItem("name"),
		email :  localStorage.getItem("email")
	};
	
	$scope.currentUser = {};
	
	/* 	Checks if attendee exists and 
	*	1. Returns id if exists
	*	2. Creates attendee and returns id
	*/
	function AttendeeExists() {
		console.log("name - "+personInfo.name);
		console.log("email - "+personInfo.email);
		
		$http.get("/attendee/email/" + personInfo.email + "/")
		
		.success(function(id) {
			console.log("ID from GET req = " + id);
			if(id == -1) {
				$http.post("/attendee", {
					name : personInfo.name,
					email : personInfo.email
				}).success(function(newdata,status,headers) {	//newdata contains attendee json
					alert("Catchup account created for "+ newdata.name);
					$scope.currentUser = newdata;
					console.log("$scope.data = "+newdata);
				});
			}
			else {
				$scope.currentUser = {
						id: id,
						name: personInfo.name,
						email: personInfo.email
				};
			}
		})
		.then(function() {
			GetHostedMeetings($scope.currentUser.id);
			GetAttendedMeetings($scope.currentUser.id);
		});
		
		//GetHostedMeetings($scope.currentUser.id);
		
	}
	
	
	
	function GetHostedMeetings(id) {
		console.log(id);
		$http.get("/meeting?attendeeType=primary&attendeeId=" + id)
			.success(function(data){
				console.log(data);
				$scope.hostedMeeting = data;
				
			});
	}
	
	function GetAttendedMeetings(id) {
		$http.get("/meeting?attendeeType=secondary&attendeeId=" + id)
		.success(function(data){
			console.log(data);
			$scope.attendedMeeting = data;
			
		});
	}
	
	AttendeeExists();
	
	//GetHostedMeetings();
	
});